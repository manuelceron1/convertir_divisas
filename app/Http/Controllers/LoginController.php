<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Hash;
use Auth;

class LoginController extends Controller
{
    public function login()
    {
        return view('pages/auth/login');
    }
    public function authenticate(Request $request)
    {
        $user = User::where('email',$request->email)->first();
        if ($user && Hash::check($request->password, $user->password)) {
            $access = true;
        }else{
            $access = false;
        }

        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        if (Auth::attempt($credentials) && $access) {
            $request->session()->regenerate();
            return redirect()->intended('/');
        }

        return back()->withErrors([
            'email' => 'Los datos ingresados no son correctos.',
        ]);
    }

    public function register()
    {
        return view('pages/auth/register');
    }
    public function store(Request $request)
    {
        $user = new User();
        $user->fill($request->all());
        $user->password = bcrypt($request->password);
        $result = $user->save();
        return $result;
    }
    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect('/login');
    }
}
