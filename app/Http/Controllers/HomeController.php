<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use AmrShawky\LaravelCurrency\Facade\Currency;

class HomeController extends Controller
{
    function index(Request $request){
        $exchange = 0;
        $from = "";
        $to = "";
        $amount = 1;
        $round = 2;
        if($request->exists('from') && $request->from!='' && $request->exists('to') && $request->to != ''){
            $from = $request->from;
            $to = $request->to;
            $amount = $request->exists('amount') ? $request->amount : 1;
            $round = $request->exists('round') ? $request->round : 2;

            $exchange = Currency::convert()
            ->from($from)
            ->to($to)
            ->amount($amount)
            ->round($round)
            ->get();
            
        }
        return view('home',[
            'exchange' => $exchange,
            'from' => $from,
            'to' => $to,
            'amount' => $amount,
            'round' => $round
        ]);
    }
}
