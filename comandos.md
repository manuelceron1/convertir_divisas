composer create-project laravel/laravel .

#crear base de datos en mysql y configurar archivo .env
#iniciar repositorio git
git init
git config --global user.name "Manuel Cerón"
git config --global user.email "manuelceron1@gmail.com"
git branch -M main
git remote add origin https://gitlab.com/manuelceron1/convertir_divisas.git
#primer commit
git add .
git commit -m "Instalar Laravel"
git push -u origin main
#migraciones
php artisan migrate 
#Controladores
php artisan make:controller UserController --resource
php artisan make:controller HomeController
php artisan make:controller LoginController
php artisan make:controller ExchangeController

#Commit Base Módulo de Usuarios
git add .
git commit -m "Base Módulo de Usuarios"
git push -u origin main

#Commit Base Módulo de Login
git add .
git commit -m "Base Login Completo"
git push -u origin main


php artisan make:controller ExchangeController

php artisan optimize
