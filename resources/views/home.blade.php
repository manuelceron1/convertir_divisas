@extends('layouts.app')
@section('content')
<!-- Exchange Form -->
<div class="row">
    <div class="col-md-1">
    </div>
    <div class="col-md-6">
        <form method="post" action="{{ route('home') }}" autocomplette="off">
        @csrf
        <div class="mb-3">
            <label for="from" class="form-label">Modena Origen</label>
            <input type="from" 
                    value="{{ $from }}" 
                    class="form-control @error('from') is-invalid @enderror" 
                    id="from" 
                    name="from" 
                    aria-describedby="fromHelp">
                @error('from')
                <div class="invalid-feedback">
                {{ $message }}
                </div>
                @enderror
            <div id="fromHelp" class="form-text">Moneda de origen para conversión.</div>
        </div>

        <div class="mb-3">
            <label for="to" class="form-label">Modena Destino</label>
            <input type="to" 
                    value="{{ $to }}" 
                    class="form-control @error('to') is-invalid @enderror" 
                    id="to" 
                    name="to" 
                    aria-describedby="toHelp">
                @error('to')
                <div class="invalid-feedback">
                {{ $message }}
                </div>
                @enderror
            <div id="toHelp" class="form-text">Moneda de destino para conversión.</div>
        </div>

        <div class="mb-3">
            <label for="amount" class="form-label">Canidad</label>
            <input type="amount" 
                    value="{{ $amount }}" 
                    class="form-control @error('amount') is-invalid @enderror" 
                    id="amount" 
                    name="amount" 
                    aria-describedby="amountHelp">
                @error('amount')
                <div class="invalid-feedback">
                {{ $message }}
                </div>
                @enderror
            <div id="amountHelp" class="form-text">Cantidad de dinero para conversión.</div>
        </div>

        <div class="mb-3">
            <label for="round" class="form-label">Decimales</label>
            <input type="round" 
                    value="{{ $round }}" 
                    class="form-control @error('round') is-invalid @enderror" 
                    id="round" 
                    name="round" 
                    aria-describedby="roundHelp">
                @error('round')
                <div class="invalid-feedback">
                {{ $message }}
                </div>
                @enderror
            <div id="roundHelp" class="form-text">Cantidad de decimales para conversión.</div>
        </div>
        
        <input type="submit" class="fadeIn fourth" value="Convertir">
        </form>

    </div>
    <div class="col-md-5">
    <h1>
        {{ $exchange }}
    </h1>
    </div>
</div>


@endsection