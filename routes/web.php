<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\UserController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Escritorio o Página de inicio */
/* Módulo de Conversión de Divisas */
Route::match(
    ['GET', 'POST'],
    '/',
    [HomeController::class, 'index']
)->name('home');
Route::post(
    '/',
    [HomeController::class, 'index']
)->name('post_home');

/* Módulo de Login */
    /* Formulario de inicio */
    Route::get(
        '/login',
        [LoginController::class, 'login']
    )->name('login');

    /* Inicio de sesión usuario */
    Route::post(
        '/login',
        [LoginController::class, 'authenticate']
    )->name('authenticate_login');

    /* Formulario de registro */
    Route::get(
        '/register',
        [LoginController::class, 'register']
    )->name('register');

    /* Registro de nuevo usuario */
    Route::post(
        '/register',
        [LoginController::class, 'store']
    )->name('store_register');
    /* Cerrar Sesión*/
    Route::post(
        '/logout',
        [LoginController::class, 'logout']
    )->name('logout');

/* Módulo de Usuarios */
    /* Listar Usuarios */
    Route::get(
        '/users',
        [UserController::class, 'index']
    )->name('users.index');
     /* Detalle de Usuario */
    Route::get(
        '/users/{id}',
        [UserController::class, 'show']
    )->name('users.show');
    /* Crear Usuarios */
    Route::post(
        '/users/create',
        [UserController::class, 'create']
    )->name('users.create');
    /* Guardar Usuarios */
    Route::post(
        '/users',
        [UserController::class, 'store']
    )->name('users.store');
    /* Editar Usuarios */
    Route::get(
        '/users/{id}',
        [UserController::class, 'edit']
    )->name('users.edit');
    /* Guardar Usuarios */
    Route::put(
        '/users/{id}',
        [UserController::class, 'update']
    )->name('users.update');
    /* Guardar Usuarios */
    Route::delete(
        '/users/{id}',
        [UserController::class, 'destroy    ']
    )->name('users.delete');
